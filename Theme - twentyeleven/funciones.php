<?php

	function conexion(){
		$conexion = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
		$conexion->SET_CHARSET("utf8"); 
		if (mysqli_connect_errno()){
			die("Error grave: " . mysqli_connect_error());
		}
		return $conexion;
	}


	function noticiaPortada(){
		$conexion = conexion();	
		$sql = "SELECT * FROM wp_posts WHERE ID in (SELECT post FROM wp_portada WHERE importancia = 1 ) ORDER BY ID DESC";	
		if($resultado = $conexion->query($sql)){
			if($row = $resultado->fetch_array(MYSQLI_ASSOC)) {	
				echo '<style type="text/css">';
				echo '#imagenPortada{ background: url('.imagenDestacada($row['ID']).'); }';
				echo '</style>';
				echo '<div id="portada">';
				echo '<div id=imagenPortada></div>';	
				echo '<article>';		
						echo '<h1>'.$row['post_title'].' ';
						echo ' <a href="'.$row['guid'].'">Seguir leyendo</a></h1>';
				echo '</article>';
				echo '</div>';			
			}
		}else{
			echo "error";
		}
		
	}


	function otrasNoticias(){
		$conexion = conexion();	
		$sql = "SELECT * FROM wp_posts WHERE ID in (SELECT post FROM wp_portada WHERE importancia = 2 ) ORDER BY ID DESC";	
		if($resultado = $conexion->query($sql)){
			while ($row = $resultado->fetch_array(MYSQLI_ASSOC)) {				
				echo '<article>';
					echo '<img src='.imagenDestacada($row['ID']).' >';		
					echo '<h1>'.$row['post_title'].'</h1>';
					$algo = strip_tags($row['post_content']);
					$cadena = explode(" ",$algo);
					echo '<p>';
					for($x=0; $x<30; $x++){
						echo $cadena[$x]." ";
					}
					echo '[...]';
					echo '<br/><a href="'.$row['guid'].'">Seguir leyendo</a></p>';
				echo '</article>';							
			}
		}else{
			echo "error";
		}
	}


	function noticiasLargas(){
		$conexion = conexion();	
		$sql = "SELECT * FROM wp_posts WHERE ID in (SELECT post FROM wp_portada WHERE importancia = 3 ) ORDER BY ID DESC";	
		if($resultado = $conexion->query($sql)){
			while ($row = $resultado->fetch_array(MYSQLI_ASSOC)) {					
				echo '<article>';
					echo '<h1>'.$row['post_title'].'</h1>';
					echo '<img src='.imagenDestacada($row['ID']).' >';					
					$algo = strip_tags($row['post_content']);
					$cadena = explode(" ",$algo);
					echo '<p>';
					for($x=0; $x<40; $x++){
						echo $cadena[$x]." ";
					}
					echo '[...]';
					echo '<br/><a href="'.$row['guid'].'">Seguir leyendo</a></p>';
				echo '</article>';					
			}
		}else{
			echo "error";
		}
	}
	

	


	function imagenDestacada($id){		
		$conexion = conexion();		
		$sql= "SELECT guid FROM wp_posts WHERE post_parent = ".$id." and post_type = 'attachment';";
		if($mini = $conexion->query($sql)){
			if($rowmini = $mini->fetch_array()){				
				return $rowmini[0];
			}else{
				return "./images/default.png";
			}
		}else{
			return "./images/default.png";
		}
	}

?>