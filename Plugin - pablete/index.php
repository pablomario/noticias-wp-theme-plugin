<?php
	
/*
Plugin Name: Gestion de Portada
Description: Plugin para gestionar las noticias que se desean visualizar
Author: Pablo Mario Garcia
Author URI: http://bytelchus.com
Version: 1.0.0
*/
	




function mt_add_pages() {
   // The first parameter is the Page name(admin-menu), second is the Menu name(menu-name)
   //and the number(5) is the user level that gets access
    add_menu_page('admin-menu', 'Gestion de Portadas', 5, __FILE__, 'mi_plugin');
}



function mi_plugin() {
	include('pablo.php');	
	
	$plugin_url = plugins_url('/pablete');

	miEstilo();
	echo '<div class="wrap">'; 
	echo '<h1>Gestion de Portadas - <span>Pablo Mario</span></h1>';
	echo '<a href="http://bytelchus.com"><h3>Bytelchus.com</h3></a>	<hr/>';


	// NOTICIAS PORTADA
	echo '<h2> Agregar Noticia Portada </h2>';
	echo '<form action="'.$plugin_url.'/agregarNoticia.php?t=1" method="POST">';	
	echo '<h3>Principal</h3>';
	echo '<select name="id">';
	echo '<option value=-1>Elige una opcion</option>';
		desplegable();
	echo '</select>';
	echo '<br/><br/><input class="button button-primary" type="submit" value="Agregar">';
	echo '</form>';
	echo '<div class="listado"><h3>Publicadas Actualmente</h3>';
		eliminarPortada();	
	echo '</div>';


	//NOTICIAS DESTACADAS
	echo '<h2> Otras Noticias </h2>';
	echo '<form action="'.$plugin_url.'/agregarNoticia.php?t=2" method="POST">';	
	echo '<h3>Agregar Noticia Destacada</h3>';
	echo '<select name="id">';
	echo '<option value=-1>Elige una opcion</option>';
		desplegable();
	echo '</select>';	
	echo '<br/><br/><input class="button button-primary" type="submit" value="Agregar">';
	echo '</form>';
	echo '<div class="listado"><h3>Publicadas Actualmente</h3>';
		eliminarDestacadas();
	echo '</div>';


	// NOTICIAS LARGAS
	echo '<h2> Noticias Largas </h2>';
	echo '<form action="'.$plugin_url.'/agregarNoticia.php?t=3" method="POST">';
	echo '<h3>Agregar Noticia Larga</h3>';	
	echo '<select name="id">';
	echo '<option value=-1>Elige una opcion</option>';
		desplegable();
	echo '</select>';	
	echo '<br/><br/><input class="button button-primary" type="submit" value="Agregar">';
	echo '</form>';	
	echo '<div class="listado"><h3>Publicadas Actualmente</h3>';
		eliminarLargas();
	echo '</div>';

	// FIN DEL DIV GENERAL
	echo '</div>';	


}






add_action('admin_menu', 'mt_add_pages');

?>




