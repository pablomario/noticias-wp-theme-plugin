<?php


	function miEstilo(){
		echo "
		<style type='text/css'>

		h1{
			font-size: 3em;
		}

		h1 span{
			color: #929292;		
		}

		a h3{
			color: #0986ba!important;
		}

		h2 {
			background: #0986ba;		
			color: #fff;
			font-weight: bolder;
			text-transform: uppercase;
			margin-top: 2em!important;
			text-indent: 10px;
		}

		input[type='text']{
			border: 1px solid #424242!important;
			padding: 5px;
			margin: 1em 0em;
			display: block;
			width: 400px;
		}

		input[type='submit']{		
			width: 220px;
			font-size: 1em!important;
			height: 30px!important;
		}

		.listado{
			padding: 10px;
		}

		.listado li{
			list-style: none;
			padding-left: 20px;
		}

		.listado span{		
			padding: 10px;
		}

		.eliminar{
			color: red!important;
		}


		</style>
		";
	}


	function conexion(){
		$conexion = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
		if (mysqli_connect_errno()){
			die("Error grave: " . mysqli_connect_error()." Los datos son ".DB_HOST);
		}
		return $conexion;
	}


	function cuantasEntradas(){
		$conexion = conexion();	
		$sql = "SELECT COUNT(*) FROM wp_posts WHERE post_status = 'publish';";	
		if($chorizo = $conexion->query($sql)){
			if($registro = $chorizo->fetch_array()){
				echo "numero de entradas: ". $registro[0];
			}
			else{
				echo "error";
			}
		}
	}

	function verEntradas(){
		$conexion = conexion();	
		$sql = "SELECT * FROM wp_posts WHERE post_status = 'publish';";	
		if($resultado = $conexion->query($sql)){
			while ($row = $resultado->fetch_array(MYSQLI_ASSOC)) {
				echo "<h1>". $row['post_title']."</h1>";			
				$content = explode(" ",$row['post_content']);
				for($x=0;$x<10;$x++){
					echo $content[$x]." ";
				}
				echo "[...] <br><a href=".$row['guid'].">Sigue Leyendo</a>";
				echo "<br>";
			}
		}else{
			echo "error Entradas";
		}
	}


	function desplegable(){
		$conexion = conexion();	
		echo "HOLA";
		$sql = "SELECT ID, post_title FROM wp_posts WHERE post_status = 'publish';";	
		if($resultado = $conexion->query($sql)){
			while ($row = $resultado->fetch_array(MYSQLI_ASSOC)) {		
				echo "<option value=".$row['ID'].">".$row['post_title']."</option>";			
			}
		}else{
			echo "error";
		}	 
	}



	function eliminarPortada(){
		$conexion = conexion();	
		$sql = "SELECT * FROM wp_posts WHERE ID in (SELECT post FROM wp_portada WHERE importancia = 1 ) ORDER BY ID DESC";	
		if($resultado = $conexion->query($sql)){
			while ($row = $resultado->fetch_array(MYSQLI_ASSOC)) {	
				$plugin_url = plugins_url('/pablete');
				$caca = $plugin_url.'/agregarNoticia.php?t=7&po='.$row['ID'];				
				echo '<li>';		
					echo '<span>'.$row['post_title'].' </span>';
					echo '<a class="eliminar" href="'.$caca.'">Eliminar</a></h1>';				
				echo '</li>';					
			}
		}else{
			echo "error";
		}

	}

	function eliminarDestacadas(){
		$conexion = conexion();	
		$sql = "SELECT * FROM wp_posts WHERE ID in (SELECT post FROM wp_portada WHERE importancia = 2 ) ORDER BY ID DESC";		
		if($resultado = $conexion->query($sql)){
			while ($row = $resultado->fetch_array(MYSQLI_ASSOC)) {			
				$plugin_url = plugins_url('/pablete');
				$caca = $plugin_url.'/agregarNoticia.php?t=7&po='.$row['ID'];				
				echo '<li>';		
					echo '<span>'.$row['post_title'].' </span>';
					echo '<a class="eliminar" href="'.$caca.'">Eliminar</a></h1>';				
				echo '</li>';					
			}
		}else{
			echo "error";
		}
	}

	function eliminarLargas(){
		$conexion = conexion();	
		$sql = "SELECT * FROM wp_posts WHERE ID in (SELECT post FROM wp_portada WHERE importancia = 3 ) ORDER BY ID DESC";	
		if($resultado = $conexion->query($sql)){
			while ($row = $resultado->fetch_array(MYSQLI_ASSOC)) {			
				$plugin_url = plugins_url('/pablete');
				$caca = $plugin_url.'/agregarNoticia.php?t=7&po='.$row['ID'];				
				echo '<li>';		
					echo '<span>'.$row['post_title'].' </span>';
					echo '<a class="eliminar" href="'.$caca.'">Eliminar</a></h1>';				
				echo '</li>';						
			}
		}else{
			echo "error";
		}
	}




?>